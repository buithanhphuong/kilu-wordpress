# Kilu - Wordpress

## FE  
- Tạo một Form có UI như [hình](https://imgur.com/XD5osxI) (Không cần giống nội dung (giá xe - hãng xe ...) - Chỉ cần có hai button với text là Books và Reviews). 
- Khi Click vào button thì sễ mở một dropdown bên dưới và request đến api /books hoặc /reviews tương ứng với từng button để lấy data về và render vào dropdown đó để user có thể chọn.  
- Sau khi chọn book thì đóng dropdown, text ban đầu của button đó sẽ thay bằng title của book /review vừa chọn và id hoặc isbn hoặc index của book / review được lưu vào form.  
- Sử dụng API của web này : https://demo.api-platform.com/  
- Khi nhấn button submit thì sẽ load lại trang hiện tại có chứa các Query trên
- Có thể dùng Jquery - Bootstrap (nên dùng)  

## BE  
Viết một function sử dụng WP_Query của Wordpress để lấy dữ liệu post.  
WP_Query có thể filter dựa theo dữ liệu lấy từ Query Strings trên URL (nếu có), gồm có:  
- Keyword  
- Giá bắt đầu - Giá kết thúc (post meta)  
- Hãng xe (category)  
- Mẫu xe  (child category)
- Năm sản xuất (trong khoảng hoặc năm chính xác) (taxonomy = year_tag)  
- Người bán (tìm qua id người bán) (post meta - meta_key = 'seller_id') 
- Thành phố (taxonomy = area_tag)  
- Quận (taxonomy = area_tag)
- Sắp xếp (Theo giá hoặc theo ngày) 


## MySQL  

- Có ba Table: seller - post - post_meta (post và post_meta là hai bảng mặc định của Wordpress) (Seller có thể có nhiều post, quan hệ của post và seller được quy định ở bảng post_meta thông qua meta_key và meta_value )  
- Trong đó post có một post meta là seller_id   
- Viết Query để lấy dữ liệu các posts thuộc seller có ID = 10   

**Sau khi hoàn thành thì upload source code lên Gitlab hoặc Github và gửi link repository về Email của anh Đăng**
